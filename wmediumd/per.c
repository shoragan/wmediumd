/*
 * Generate packet error rates for OFDM rates given signal level and
 * packet length.
 */

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "wmediumd.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

/* Code rates for convolutional codes */
enum fec_rate {
	FEC_RATE_1_2,
	FEC_RATE_2_3,
	FEC_RATE_3_4,
};

struct rate {
	int mbps;
	int mqam;
	enum fec_rate fec;
};

/*
 * rate sets are defined in drivers/net/wireless/mac80211_hwsim.c#hwsim_rates.
 */
static struct rate rateset[] = {
	/*
	 * XXX:
	 * For rate = 1, 2, 5.5, 11 Mbps, we will use mqam and fec of closest
	 * rate. Because these rates are not OFDM rate.
	 */
	{ .mbps = 10, .mqam = 2, .fec = FEC_RATE_1_2 },
	{ .mbps = 20, .mqam = 2, .fec = FEC_RATE_1_2 },
	{ .mbps = 55, .mqam = 2, .fec = FEC_RATE_1_2 },
	{ .mbps = 110, .mqam = 4, .fec = FEC_RATE_1_2 },
	{ .mbps = 60, .mqam = 2, .fec = FEC_RATE_1_2 },
	{ .mbps = 90, .mqam = 2, .fec = FEC_RATE_3_4 },
	{ .mbps = 120, .mqam = 4, .fec = FEC_RATE_1_2 },
	{ .mbps = 180, .mqam = 4, .fec = FEC_RATE_3_4 },
	{ .mbps = 240, .mqam = 16, .fec = FEC_RATE_1_2 },
	{ .mbps = 360, .mqam = 16, .fec = FEC_RATE_3_4 },
	{ .mbps = 480, .mqam = 64, .fec = FEC_RATE_2_3 },
	{ .mbps = 540, .mqam = 64, .fec = FEC_RATE_3_4 },
};
static size_t rate_len = ARRAY_SIZE(rateset);

int index_to_rate(size_t index, u32 freq)
{
	if (freq > 5000)
		index += 4;

	if (index >= rate_len)
		index = rate_len - 1;

	return rateset[index].mbps;
}
