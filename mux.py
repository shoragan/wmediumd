#!/usr/bin/python3

import asyncio
import struct
from time import time
from math import modf
from glob import glob

class Mux:
    def __init__(self, loop):
        self.loop = loop
        self.writers = []
        self.pcap_writers = []

    async def start(self):
        self.server = await asyncio.start_unix_server(
                self.client_connected, path = 'server.socket')

        # use with 'wireshark -i pcap.socket -k'
        self.pcap_server = await asyncio.start_unix_server(
                self.pcap_connected, path = 'pcap.socket')

        self.monitor_task = asyncio.ensure_future(self.monitor(
            "/tmp/*.hwsim"))

    async def monitor(self, pattern):
        active = {}
        while True:
            await asyncio.sleep(1)

            files = glob(pattern)
            for file in files:
                if file in active:
                    continue
                print("connecting to: {}".format(file))
                reader, writer = await asyncio.open_unix_connection(
                        path=file)
                task = asyncio.ensure_future(
                        self.client_connected(reader, writer))
                active[file] = task

            dead = []
            for file, task in active.items():
                if task.done():
                    dead.append(file)
            for file in dead:
                print("disconnected from: {}".format(file))
                del active[file]


    async def client_connected(self, reader, writer):
        self.writers.append(writer)
        print("client connected: {} {}".format(reader, writer))
        while True:
            header = await reader.readexactly(4)
            size, = struct.unpack("!I", header)
            data = await reader.readexactly(size)
            ts = time()
            print("rx {}".format(size))
            dead = []
            for other in self.writers:
                if other is writer:
                    continue
                other.write(header)
                other.write(data)
                try:
                    await other.drain()
                except:
                    dead.append(other)
            for other in dead:
                print("wreiter is dead: {}".format(other))
                self.writers.remove(other)
            await self.pcap_send(data, ts)

    async def pcap_connected(self, reader, writer):
        print("pcap connected: {} {}".format(reader, writer))
        header = struct.pack("IHHiIII",
                0xa1b2c3d4,
                2, 4,
                0, 0,
                65535,
                105)
        writer.write(header)
        self.pcap_writers.append(writer)

    async def pcap_send(self, data, ts):
        ts_fract, ts_sec = modf(ts)
        ts_usec = ts_fract * 1000000
        header = struct.pack("IIII", int(ts_sec), int(ts_usec), len(data), len(data))
        dead = []
        for pcap in self.pcap_writers:
            pcap.write(header)
            pcap.write(data)
            try:
                await pcap.drain()
            except:
                dead.append(pcap)
        for pcap in dead:
            print("pcap is dead: {}".format(pcap))
            self.pcap.remove(pcap)

def main():
    loop = asyncio.get_event_loop()
    mux = Mux(loop)
    loop.run_until_complete(mux.start())
    loop.run_forever()

if __name__ == "__main__":
    main()
